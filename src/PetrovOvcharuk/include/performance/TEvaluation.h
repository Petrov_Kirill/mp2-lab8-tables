// Copyright 2016 Petrov Kirill

#ifndef INCLUDE_TEVALUATION_PETROV_H_
#define INCLUDE_TEVALUATION_PETROV_H_

#include <vector>
#include <numeric> 
#include <iostream>

#include "TDatValue.h"

enum Evaluation { 
  F = 1,
  D = 2,
  C = 3,
  B = 4,
  A = 5,
  AA = 6
};

class TEvaluation : public TDatValue {
public:
  TEvaluation(const std::vector<Evaluation>& evaluations) { evaluations_ = evaluations; }
  TEvaluation(const TEvaluation&) = delete;
  double averageRating(void) {
    double result = 0.0;
    for (size_t i = 0; i < evaluations_.size(); i++)
      result += static_cast<double>(evaluations_[i]);
    return result / evaluations_.size();
  }

  bool fellows(void) {
    for (auto item : evaluations_)
      if (item <= C) return false;
    return true;
  }

  Evaluation GetEval(int i) { return evaluations_[i]; }
  PTDatValue GetCopy() { return static_cast<PTDatValue>(new TEvaluation(evaluations_)); }
  bool operator== (const TEvaluation& ev) const { return evaluations_ == ev.evaluations_; }
  void Print(std::ostream& os) { for (auto item : evaluations_) os << item << "      "; }
private:
  std::vector<Evaluation> evaluations_;
};

typedef TEvaluation* PTEvaluation;

#endif  // INCLUDE_TEVALUATION_PETROV_H_