// Copyright 2016 Petrov Kirill

#ifndef INCLUDE_TPERFORMANCE_PETROV_H_
#define INCLUDE_TPERFORMANCE_PETROV_H_

#include <fstream>
#include <string>
#include <regex>
#include <vector>
#include <iostream>

#include "TTabRecord.h"
#include "TEvaluation.h"

using std::ifstream;
using std::vector;
using std::string;
using std::regex;
using std::smatch;
using std::getline;

typedef std::regex_iterator<string::iterator> inter;

template <class TABLE> 
class TPeformance: public TABLE {
public:
  TPeformance(ifstream& file);
  
  void printGroup(std::ostream& os);

  double averageStudent(const TKey& str);
  Evaluation findStudentEvalution(const TKey& str, const string& sub);

  double averageSubjectsGroup(const string& sub);
  double averageAllSubjectsGroup(void);
  int searchStandouts(const string& sub);
  int searchFellows();
  
  void static printSubjects(std::ostream& os);
  
private:
  int parseStudentCount(ifstream& file);
  void parseSubjects(void);

  size_t findSubj(const string sub);

  static vector<string> subjects;
  int studentCount;

};

template<class Type>
vector<string> TPeformance<Type>::subjects = {};

template<class TABLE>
inline TPeformance<TABLE>::TPeformance(ifstream & file) :TABLE(parseStudentCount(file)) {
  if (subjects.empty())
    parseSubjects();

  string buff;
  regex reg("\\S+");
  smatch match;
  vector<Evaluation> vect;
  PTEvaluation pEval = nullptr;

  for (int i = 0; i < studentCount; i++) {
    getline(file, buff);
    inter it_eval(buff.begin(), buff.end(), reg);
    TKey key = it_eval->str();
    ++it_eval;
    for ( ; it_eval != inter(); ++it_eval)
      vect.push_back(static_cast<Evaluation>(atoi(it_eval->str().c_str() ) ) );
    pEval = new TEvaluation(vect);
    vect.clear();
    TABLE::InsRecord(key, pEval);
  }
}

template<class TABLE>
inline double TPeformance<TABLE>::averageAllSubjectsGroup(void) {
  double result = 0.0;
  for (Reset(); !IsTabEnded(); GoNext())
    result += static_cast<PTEvaluation>(GetValuePTR())->averageRating();
  return result / studentCount;
}

template<class TABLE>
inline int TPeformance<TABLE>::searchStandouts(const string & sub) {
  int count = 0;
  for (Reset(); !IsTabEnded(); GoNext())
    if (static_cast<PTEvaluation>(GetValuePTR())->GetEval(findSubj(sub)) >= A)
      count++;
  return count;
}

template<class TABLE>
inline int TPeformance<TABLE>::searchFellows(void){
  int count = 0;
  for (Reset(); !IsTabEnded(); GoNext())
    if ( static_cast<PTEvaluation>(GetValuePTR())->fellows() )
      count++;
  return count;
}

template<class TABLE>
inline double TPeformance<TABLE>::averageSubjectsGroup(const string& sub) {
  double result = 0.0;
  size_t index = findSubj(sub);
  for (Reset(); !IsTabEnded(); GoNext())
    result += static_cast<PTEvaluation>(GetValuePTR())->GetEval(index);
  return result / studentCount;
}

template<class TABLE>
inline Evaluation TPeformance<TABLE>::findStudentEvalution(const TKey & str, const string & sub) {
  PTEvaluation pEval = static_cast<PTEvaluation> (TABLE::FindRecord(str));
  return pEval->GetEval(findSubj(sub));
}

template<class TABLE>
inline void TPeformance<TABLE>::printSubjects(std::ostream & os) {
  for (auto sub : subjects)
    os << sub << " ";
}

template<class TABLE>
inline void TPeformance<TABLE>::printGroup(std::ostream & os) {
  os.width(25);
  os << "Subjects:";
  printSubjects(os);
  os << "\n\n";
  for (Reset(); !IsTabEnded(); GoNext()) {
    os.width(25);
    os << GetKey();
    os << "  ";
    static_cast<PTEvaluation>(GetValuePTR())->Print(os);
    os << '\n';
  }
    
}

template<class TABLE>
inline double TPeformance<TABLE>::averageStudent(const TKey & str){
  return static_cast<PTEvaluation> (TABLE::FindRecord(str))->averageRating();
}

template<class TABLE>
inline int TPeformance<TABLE>::parseStudentCount(ifstream & file) {
  studentCount = 0;
  string buff;

  while (getline(file, buff))
    studentCount++;

  file.clear();
  file.seekg(0, std::ios_base::beg);
  return studentCount;
}

template<class TABLE>
inline void TPeformance<TABLE>::parseSubjects(void) {
  ifstream subjectsFile("discipline.txt");
  int numberSubjects = 0;
  subjectsFile >> numberSubjects;
  string buff;
  for (int i = 0; i < numberSubjects; i++) {
    subjectsFile >> buff;
    subjects.push_back(buff);
  }
}

template<class TABLE>
inline size_t TPeformance<TABLE>::findSubj(const string sub)
{
  size_t index;
  for (index = 0; index < subjects.size(); index++)
    if (subjects[index] == sub)
      break;
  return index;
}

#endif  // INCLUDE_TPERFORMANCE_PETROV_H_
