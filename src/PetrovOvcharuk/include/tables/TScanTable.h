// Copyright 2016 Ovcharuk Oleg

#ifndef INCLUDE_TSCANTABLE_H_
#define INCLUDE_TSCANTABLE_H_

#include "TArrayTable.h"

class  TScanTable : public TArrayTable {
public:
    TScanTable(int Size) : TArrayTable(Size) {}; //�����������
    // �������� ������
    virtual PTDatValue FindRecord(TKey k) override;                    //����� ������
    virtual void InsRecord(TKey k, PTDatValue pVal) override;          //��������
    virtual void DelRecord(TKey k) override;                           //������� ������

};

#endif // INCLUDE_TSCANTABLE_H_