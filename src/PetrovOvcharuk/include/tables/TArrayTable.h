// Copyright 2016 Petrov Kirill

#ifndef INCLUDE_TARRAYTABLE_H_
#define INCLUDE_TARRAYTABLE_H_


#include "TTable.h"
#include "TTabRecord.h"

class TSortTable;

enum class TDataPos { FIRST_POS, CURRENT_POS, LAST_POS };

class  TArrayTable : public TTable {
protected:
  PTTabRecord* pRecs;                     // ������ ��� ������� �������
  int TabSize;                            // ����. ����.���������� ������� � �������
  int CurrPos;                            // ����� ������� ������ (��������� � 0)
public:
  static const int TAB_MAX_SIZE = 25;
  TArrayTable(int size);         // �����������
  ~TArrayTable();                               // ����������
  TArrayTable(const TArrayTable&) = delete;
// �������������� ������
  virtual bool IsFull() const override;         // ���������?
  int GetTabSize() const;                       // �-�� �������
// ������
  virtual TKey GetKey(void) const override;
  virtual PTDatValue GetValuePTR(void) const override;
  TKey GetKey(TDataPos mode) const;
  PTDatValue GetValuePTR(TDataPos mode) const;
// �������� ������
  virtual PTDatValue FindRecord(TKey k) = 0;           // ����� ������
  virtual void InsRecord(TKey k, PTDatValue pVal) = 0; // ��������
  virtual void DelRecord(TKey k) = 0;                  // ������� ������
//���������
  virtual void Reset(void) override final;             // ���������� �� ������ ������
  virtual bool IsTabEnded(void) const override final;  // ������� ���������?
  virtual Data GoNext(void) override final;            // ������� � ��������� ������

  virtual Data SetCurrentPos(int pos);                 // ���������� ������� ������
  int GetCurrentPos(void) const;                       // �������� ����� ������� ������
  friend TSortTable;
};

#endif  // INCLUDE_TARRAYTABLE_H_
