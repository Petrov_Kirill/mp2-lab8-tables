#ifndef INCLUDE_TTABLE_H_
#define INCLUDE_TTABLE_H_

#include <string>

#include "TDataCom.h"
#include "TDatValue.h"

using std::string;

typedef string TKey;

class  TTable : public TDataCom {
protected:
  int DataCount;  // ���������� ������� � �������
  int Efficiency; // ���������� ������������� ���������� ��������
public:
  TTable():TDataCom() { DataCount = 0; Efficiency = 0; } // �����������
  virtual ~TTable() = default; // ����������
// �������������� ������
  int GetDataCount() const { return DataCount; }     // �-�� �������
  int GetEfficiency() const { return Efficiency; }   // �������������
  bool IsEmpty() const { return DataCount == 0; }    //�����?
  virtual bool IsFull() const = 0;                   // ���������?
// ������
  virtual TKey GetKey(void) const = 0;
  virtual PTDatValue GetValuePTR(void) const = 0;
// �������� ������
  virtual PTDatValue FindRecord(TKey k) = 0;           // ����� ������
  virtual void InsRecord(TKey k, PTDatValue pVal) = 0; // ��������
  virtual void DelRecord(TKey k) = 0;                  // ������� ������
// ���������
  virtual void Reset(void) = 0;                       // ���������� �� ������ ������
  virtual bool IsTabEnded(void) const = 0;            // ������� ���������?
  virtual Data GoNext(void) = 0;                      // ������� � ��������� ������
  // (=1 ����� ���������� ��� ��������� ������ �������)
};

#endif  // INCLUDE_TTABLE_H_
