// Copyright 2016 Ovcharuk Oleg

#ifndef INCLUDE_TTABRECORD_OVCHARUK_H_
#define INCLUDE_TTABRECORD_OVCHARUK_H_

#include "TDatValue.h"
#include <iostream>

typedef std::string TKey; // ��� ����� ������

// ����� ��������-�������� ��� ������� �������
class TTabRecord : public TDatValue {  
 protected:    
    // ����    
    TKey Key;            // ���� ������
    PTDatValue pValue;   // ��������� �� ��������

 public:  
    // ������
    TTabRecord(TKey k = "", PTDatValue pVal = nullptr);  // ����������� 
    TTabRecord(const TTabRecord&) = delete;
    void SetKey(TKey k);                                 // ���������� �������� �����
    TKey GetKey(void) const;                             // �������� �������� �����
    void SetValuePtr(PTDatValue p);                      // ���������� ��������� �� ������
    PTDatValue GetValuePTR(void) const;                  // �������� ��������� �� ������
    virtual TDatValue * GetCopy() override;              // ���������� �����
    TTabRecord & operator = (TTabRecord &tr);            // ������������
    virtual bool operator == (const TTabRecord &tr);     // ��������� =
    virtual bool operator < (const TTabRecord &tr);      // ��������� �<�
    virtual bool operator > (const TTabRecord &tr);      // ��������� �>�

    // ������������� ������ ��� ��������� ����� ������, ��. �����
    friend class TArrayTable;
    friend class TScanTable;
    friend class TSortTable;
    friend class TTreeNode;
    friend class TTreeTable;
    friend class TArrayHash;
    friend class TListHash;
};

typedef TTabRecord *PTTabRecord;

#endif // INCLUDE_TTABRECORD_OVCHARUK_H_