// Copyright 2016 Petrov Kirill

#ifndef INCLUDE_TDATLINK_PETROV_H_
#define INCLUDE_TDATLINK_PETROV_H_

#include "trootlink.h"

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink {
protected:
  PTDatValue pValue;  // ��������� �� ������ ��������
public:
  TDatLink(PTDatValue pVal = NULL, PTRootLink pN = NULL) :TRootLink(pN) {
    pValue = pVal;
  }
  void SetDatValue(PTDatValue pVal) { pValue = pVal; }
  PTDatValue GetDatValue() { return  pValue; }
  PTDatLink  GetNextDatLink() { return  (PTDatLink)pNext; }
  friend class TDatList;
};

#endif  // INCLUDE_TDATLINK_PETROV_H_