// Copyright 2016 Petrov Kirill

#ifndef INCLUDE_TROOTLINK_PETROV_H_
#define INCLUDE_TROOTLINK_PETROV_H_

#include <iostream>
#include "tdatvalue.h"

class TRootLink;
typedef TRootLink *PTRootLink;

class TRootLink {
protected:
  PTRootLink  pNext;  // ��������� �� ��������� �����
public:
  TRootLink(PTRootLink pN = NULL) { pNext = pN; }
  PTRootLink  GetNextLink() { return  pNext; }
  void SetNextLink(PTRootLink  pLink) { pNext = pLink; }
  void InsNextLink(PTRootLink  pLink) {
    PTRootLink p = pNext;  pNext = pLink;
    if (pLink != NULL) pLink->pNext = p;
  }
  virtual void SetDatValue(PTDatValue pVal) = 0;
  virtual PTDatValue GetDatValue() = 0;

  friend class TDatList;
};

#endif  // INCLUDE_TROOTLINK_PETROV_H_