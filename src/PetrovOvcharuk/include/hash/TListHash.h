// Copyright 2016 Petrov Kirill

#ifndef INCLUDE_TLISTHASH_PETROV_H_
#define INCLUDE_TLISTHASH_PETROV_H_

#include "THashTable.h"
#include "tdatlist.h"
#include "TTabRecord.h"

class  TListHash : public THashTable {
protected:
  PTDataList* pList;                                              // ������ ��� ������� ���������� �� ������ ������� 
  int TabSize;                                                    // ������ ������� ����������
  int CurrList;                                                   // ������, � ������� ���������� �����
public:
  TListHash(int size = TAB_MAX_SIZE);                              // �����������
  ~TListHash();
// �������������� ������
  virtual bool IsFull() const override final;                      // ���������?
// ������
  virtual TKey GetKey(void) const override final;
  virtual PTDatValue GetValuePTR(void) const override final;
// �������� ������
  virtual PTDatValue FindRecord(TKey k) override final;             // ����� ������
  virtual void InsRecord(TKey k, PTDatValue pVal) override final;   // ��������
  virtual void DelRecord(TKey k) override final;                    // ������� ������
// ���������
  virtual void Reset(void) override final;                          // ���������� �� ������ ������
  virtual bool IsTabEnded(void) const override final;               // ������� ���������?
  virtual Data GoNext(void) override final;                         // ������� � ��������� ������
};

#endif  // INCLUDE_TLISTHASH_PETROV_H_