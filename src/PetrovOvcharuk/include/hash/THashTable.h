// Copyright 2016 Petrov Kirill

#ifndef INCLUDE_THASHTABLE_PETROV_H_
#define INCLUDE_THASHTABLE_PETROV_H_

#include "TTable.h"

class  THashTable : public TTable {
protected:
  unsigned long HashFunc(const TKey& key); // hash-�������
public:
  static const int TAB_MAX_SIZE = 25;
  THashTable() : TTable() {}
};

#endif  // INCLUDE_THASHTABLE_PETROV_H_