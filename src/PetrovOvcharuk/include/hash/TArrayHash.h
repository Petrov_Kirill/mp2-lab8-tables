// Copyright 2016 Petrov Kirill

#ifndef INCLUDE_TARRAYHASH_H_
#define INCLUDE_TARRAYHASH_H_

#include "THashTable.h"
#include "TTabRecord.h"

class  TArrayHash : public THashTable {
protected:
  PTTabRecord* pRecs;                                              // ������ ��� ������� �������
  int TabSize;                                                     // ����. ����. �-�� �������
  int HashStep;                                                    // ��� ���������� �������������
  int CurrPos;                                                     // ������ ������ ��� ���������� ������
  int GetNextPos(int pos) { return (pos + HashStep) % TabSize; };  // ����. �����.
public:
  // ���������
  static const int TAB_HASH_STEP = 1;
  TArrayHash(int size);
  ~TArrayHash();
  // �������������� ������
  virtual bool IsFull() const override final;                       // ���������?
  // ������
  virtual TKey GetKey(void) const override final;
  virtual PTDatValue GetValuePTR(void) const override final;
  // �������� ������
  virtual PTDatValue FindRecord(TKey k) override final;             // ����� ������
  virtual void InsRecord(TKey k, PTDatValue pVal) override final;   // ��������
  virtual void DelRecord(TKey k) override final;                    // ������� ������
  // ���������
  virtual void Reset(void) override final;                          // ���������� �� ������ ������
  virtual bool IsTabEnded(void) const override final;               // ������� ���������?
  virtual Data GoNext(void) override final;                         // ������� � ��������� ������

};

#endif  // INCLUDE_TARRAYHASH_H_