// Copyright 2016 Ovcharuk Oleg

#ifndef TTREETABLE_OVCHARUK_H_
#define TTREETABLE_OVCHARUK_H_

#include <stack>
#include "TTable.h"
#include "TTreeNode.h"

class  TTreeTable : public TTable {
 protected:
    PTTreeNode pRoot;    // ��������� �� ������ ������
    PTTreeNode *ppRef;   // ����� ��������� �� �������-���������� � FindRecord
    PTTreeNode pCurrent; // ��������� �� ������� �������
    int CurrPos;         // ����� ������� �������
    std::stack < PTTreeNode> St; // ���� ��� ���������
    std::stack < PTTreeNode> St1; // ���� ��� ��������
    void DeleteTreeTab(PTTreeNode pNode); // ��������
 public:
    TTreeTable() : TTable() { CurrPos = 0; pRoot = pCurrent = nullptr; ppRef = nullptr; }
    TTreeTable(int size) : TTreeTable() {};
    ~TTreeTable() { DeleteTreeTab(pRoot); } // ����������
// �������������� ������
    virtual bool IsFull() const override final; //���������?
// �������� ������
    virtual PTDatValue FindRecord(TKey k) override final;     // ����� ������
    virtual void InsRecord(TKey k, PTDatValue pVal) override; // ��������
    virtual void DelRecord(TKey k) override;                  // ������� ������
// ���������
    virtual TKey GetKey(void) const override final;
    virtual PTDatValue GetValuePTR(void) const override final;
    virtual void Reset(void) override final;            // ���������� �� ������ ������ (����� ����� �������?)
    virtual bool IsTabEnded(void) const override final; // ������� ���������?
    virtual Data GoNext(void) override final;           // ������� � ��������� ������
    //(=1 ����� ���������� ��� ��������� ������ �������)
};

#endif // TTREETABLE_OVCHARUK_H_