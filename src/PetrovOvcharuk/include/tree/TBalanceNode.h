// Copyright 2016 Ovcharuk Oleg

#ifndef TBALANCENODE_OVCHARUK_H_
#define TBALANCENODE_OVCHARUK_H_

#include "TTreeNode.h"

enum class Bal { BalOK, BalLeft, BalRight};

class  TBalanceNode : public TTreeNode {
protected:
    Bal Balance; // индекс балансировки вершины (-1,0,1)
public:
    TBalanceNode(TKey k = "", PTDatValue pVal = NULL, PTTreeNode pL = NULL,
        PTTreeNode pR = NULL, Bal bal = Bal::BalOK ): TTreeNode(k, pVal, pL, pR),
        Balance(bal) {}; // конструктор
    virtual TDatValue * GetCopy();  // изготовить копию
    Bal GetBalance(void) const;
    void SetBalance(Bal bal);
    friend class TBalanceTree;
};

typedef TBalanceNode *PTBalanceNode;

#endif // TBALANCENODE_OVCHARUK_H_