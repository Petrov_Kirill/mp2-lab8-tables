// Copyright 2016 Ovcharuk Oleg

#ifndef TBALANCETREE_OVCHARUK_H_
#define TBALANCETREE_OVCHARUK_H_

#include "TTreeTable.h"
#include "TBalanceNode.h"

enum class Height {OK, Inc};

class  TBalanceTree : public TTreeTable {
 protected:
    Height InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal);
    Height LeftTreeBalancing(PTBalanceNode &pNode);  // ������. ������ ���������
    Height RightTreeBalancing(PTBalanceNode &pNode); // ������. ������� ���������
 public:
    TBalanceTree() :TTreeTable() {} // �����������
    TBalanceTree(int size) :TBalanceTree() {};
 //�������� ������
    virtual void InsRecord(TKey k, PTDatValue pVal) override final; // ��������
    virtual void DelRecord(TKey k) override final;                  // �������
};

typedef TBalanceTree *PTBalanceTree;

#endif // TBALANCETREE_OVCHARUK_H_