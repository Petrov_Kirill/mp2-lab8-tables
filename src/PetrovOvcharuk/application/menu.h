// Copyright 2016 Ovcharuk Oleg

#ifndef INCLUDE_OVCHARUK_MENU_H_
#define INCLUDE_OVCHARUK_MENU_H_

#include <iostream>
#include <algorithm>

#include "TPerformance.h"

#include "TScanTable.h"
#include "TArrayHash.h"
#include "TListHash.h"
#include "TBalanceTree.h"
#include "TTreeTable.h"
#include "TSortTable.h"

typedef TPeformance<TScanTable> Table1;
typedef TPeformance<TSortTable> Table2;
typedef TPeformance<TBalanceTree> Table3;
typedef TPeformance<TListHash> Table4;
typedef TPeformance<TArrayHash> Table5;

class Menu {
private:
  char hold = 'n';
  int CurrentID;
  TKey CurrentStudent;
  string NameGroup;

  static const int MAX_GROUP = 4;
  
  const string Names[MAX_GROUP] = { "group0828", "group0829", "group0820", "group0821" };

  Table3* Groups[MAX_GROUP];
  
  void setUp() {
    CurrentID = 1;
    NameGroup = Names[CurrentID-1];
    ifstream files[MAX_GROUP];
    for (int i = 0; i < MAX_GROUP; i++) {
      files[i].open(Names[i] + ".txt");
      Groups[i] = new Table3(files[i]);
    }
  }

  void statusMain() {
    system("cls");
    std::cout << "� ������ ������ �� ��������� � " << CurrentID << " �������� �� 4" << std::endl;
    std::cout << "����� ������: " << NameGroup << std::endl;
  }

  void statusStud() {
    system("cls");
    std::cout << "� ������ ������ �� ��������� �� ��������� " << CurrentStudent << std::endl;
  }

  void errorIn() {
    std::cout << "������, �������� ������! " << std::endl;
  }

  void continueWork() {
    while (hold != 'y') {
      std::cout << "���������� ������? (y/n) ";
      std::cin >> hold;
    }
  }

  void switchTable(int tab) {
    ifstream file; (NameGroup + ".txt");
    switch (tab) {
    case 1:
      CurrentID = 1;
      NameGroup = Names[CurrentID - 1];   
      break;
    case 2:
      CurrentID = 2;
      NameGroup = Names[CurrentID - 1];
      break;
    case 3:  
      CurrentID = 3;
      NameGroup = Names[CurrentID - 1];
      break;
    case 4:    
      CurrentID = 4;
      NameGroup = Names[CurrentID - 1]; 
      break;
    default:
      errorIn();
      break;
    }
    file.close();
    
  }

  bool findStud(TKey k) {
    return Groups[CurrentID - 1]->FindRecord(k) == nullptr ? false : true;
  }

  void printSubjects() {
    Groups[0]->printSubjects(std::cout);
    std::cout << std::endl;
    std::cout << "������� ������ �������: ";
  }

  void printStudMenu() {
    std::cout << "1. �������� ������������ �� ����������� ��������;" << std::endl
      << "2. �������� ������� ������ �� ���� ���������;" << std::endl
      << "3. ��������� � ������ � ��������. " << std::endl
      << "�������� ������: ";
  }

  void workStud() {
    int ch1=1;
    string subj;
    while (ch1 != 3) {
      statusStud();
      printStudMenu();
      std::cin >> ch1;
      switch (ch1) {
      case 1:
        printSubjects();
        std::cin >> subj;
        std::cout << "������ �� " << subj << ": ";
        std::cout << Groups[CurrentID - 1]->findStudentEvalution(CurrentStudent, subj) << std::endl;
        hold = 'n';
        break;
      case 2:
        std::cout << "������� ���� ��������: ";
        std::cout << Groups[CurrentID - 1]->averageStudent(CurrentStudent)<<std::endl;
        hold = 'n';
        break;
      case 3:
        hold = 'y';
        break;
      default:
        errorIn();
        hold = 'n';
        break;
      }
      continueWork();
    }
  }

  void printGroupMenu() {
    std::cout << "1. ������� ������� ������������ ������ �� ��������;" << std::endl
      << "2. ������� ������� ������������ ������ �� ���� ���������;" << std::endl
      << "3. ������� ���������� ���������� �� ��������;" << std::endl
      << "4. ������� ���������� ���������, ���������� ���������; " << std::endl
      << "5. ��������� � ������ � ��������." << std::endl
      << "�������� ������: ";
  }
  
  void workGroup() {
    int ch2=1;
    string subj;
    while (ch2 != 5) {
      statusMain();
      printGroupMenu();
      std::cin >> ch2;
      switch (ch2) {
      case 1:
        printSubjects();
        std::cin >> subj;
        std::cout << "������� ���� ������ �� ��������� ��������: " << Groups[CurrentID - 1]->averageSubjectsGroup(subj)<< std::endl;
        hold = 'n';
        break;
      case 2:
        std::cout << "������� ������������ ������: " << Groups[CurrentID - 1]->averageAllSubjectsGroup() << std::endl;
        hold = 'n';
        break;
      case 3:
        printSubjects();
        std::cin >> subj;
        std::cout << "����� ���-�� ���������� �� " << subj << " ��������." << std::endl;
        std::cout << "���������� ����������: " << Groups[CurrentID - 1]->searchStandouts(subj) << std::endl;
        hold = 'n';
        break;
      case 4:
        std::cout << "���-�� �����, ���������� ���������: ";
        std::cout << Groups[CurrentID - 1]->searchFellows() << std::endl;
        hold = 'n';
        break;
      case 5:
        hold = 'y';
        break;
      default:
        errorIn();
        hold = 'n';
        break;
      }
      continueWork();
    }
  }

  void printCompareMenu() {
    std::cout << "1. ������� ������� ������ ����� �� ��������; " << std::endl
      << "2. ������� ������� ������ ����� �� ���� ���������;" << std::endl
      << "3. ������� ���-�� ���������� ��������� �� ���� �������;" << std::endl
      << "4. ����������� ������ ������ �� ��������;" << std::endl
      << "5. ��������� � ������ � ��������." << std::endl
      << "�������� ������: ";
  }

  void workCompare() {
    int ch3=1;
    string subj;
    while (ch3 != 5) {
      system("cls");
      printCompareMenu();
      std::cin >> ch3;
      double max = 0.0;
      string buf;
      switch (ch3) {
      case 1:
        printSubjects();
        std::cin >> subj;
        for (int i = 0; i < MAX_GROUP; i++) 
          std::cout << Names[i] << ": " << Groups[i]->averageSubjectsGroup(subj) << std::endl;
        hold = 'n';
        break;
      case 2:
        for (int i = 0; i < MAX_GROUP; i++)
          std::cout << Names[i] << ": " << Groups[i]->averageAllSubjectsGroup()<< std::endl;
        hold = 'n';
        break;
      case 3:
        for (int i = 0; i < MAX_GROUP; i++)
          std::cout << Names[i] << ": " << Groups[i]->searchFellows() << std::endl;
        hold = 'n';
        break;
      case 4:
        printSubjects();
        std::cin >> subj;
        
        for (int i = 0; i < MAX_GROUP; i++)
          if (Groups[i]->averageSubjectsGroup(subj) > max) {
            max = Groups[i]->averageSubjectsGroup(subj);
            buf = Names[i];
          }
        std::cout << "������ ������: " << buf << "(" << max << ")" << std::endl;
        hold = 'n';
        break;
      case 5:
        hold = 'y';
        break;
      default:
        errorIn();
        hold = 'n';
        break;
      }
      continueWork();
    }
  }

  void printMainMenu() {
    std::cout << "1. ������� ������� �� �����;" << std::endl
      << "2. �������� ���������� ��������;" << std::endl
      << "3. �������� ���������� ������;" << std::endl
      << "4. ������������� �� ������ ������;" << std::endl
      << "5. ������������� ���������� ���� �����;" << std::endl
      << "6. ��������� ������." << std::endl
      << "�������� ������: ";
  }

public:
  Menu() = default;
  void workMain() {
    setUp();
    int ch=1;
    while (ch != 6) {
      statusMain();
      printMainMenu();
      std::cin >> ch;
      TKey k;
      int gr;
      switch (ch) {
      case 1:
        Groups[CurrentID - 1]->printGroup(std::cout);
        hold = 'n';
        break;
      case 2:
        std::cout << "������� ��� ��������: ";
        std::cin >> k;
        if (findStud(k)) {
          CurrentStudent = k;
          workStud();
        }
        else {
          errorIn();
        }
        hold = 'n';
        break;
      case 3:
        workGroup();
        hold = 'y';
        break;
      case 4:
        std::cout << "������� ����� ������� (1-4) "; std::cin >> gr;
        switchTable(gr);
        hold = 'y';
        break;
      case 5:
        workCompare();
        hold = 'y';
        break;
      case 6:
        hold = 'y';
        break;
      default:
        errorIn();
        hold = 'n';
        break;
      }
      continueWork();
    }
    std::cout << "����� �������! ";
  }
};

#endif  // INCLUDE_OVCHARUK_MENU_H_