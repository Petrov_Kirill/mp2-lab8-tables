// Copyright 2016 Ovcharuk Oleg 

#include "TScanTable.h"

PTDatValue TScanTable::FindRecord(TKey k)
{
    for (int i = 0; i < TabSize; i++) {
      if (pRecs[i] != nullptr)
        if (pRecs[i]->GetKey() == k) {
            return pRecs[i]->GetValuePTR();
        }
    }
    SetRetCode(Data::NO_RECORD);
    return nullptr;
}

void TScanTable::InsRecord(TKey k, PTDatValue pVal)
{
    SetRetCode(Data::OUT_OF_RANGE);
    for (int i = 0; i < TabSize; i++) {
        if (pRecs[i] == nullptr) {
            pRecs[i] = new TTabRecord(k, pVal);
            DataCount++;
            SetRetCode(Data::OK);
            break;
        }
    }
}

void TScanTable::DelRecord(TKey k)
{
    SetRetCode(Data::NO_RECORD);
    for (int i = 0; i < TabSize; i++) {
      if (pRecs[i] != nullptr)
        if (pRecs[i]->GetKey() == k) {
            delete pRecs[i];
            pRecs[i] = nullptr;
            DataCount--;
            SetRetCode(Data::OK);
        }
    }
}
