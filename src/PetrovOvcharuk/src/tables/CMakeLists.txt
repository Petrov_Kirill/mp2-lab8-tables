set(target "tables")

file(GLOB hrcs ../../${INCLUDE}/*h ../../${INCLUDE}/tables/*h)
file(GLOB srcs "*.cpp")

# Add library this
add_library(${target} STATIC ${SOURCES} ${hrcs}  ${srcs} )