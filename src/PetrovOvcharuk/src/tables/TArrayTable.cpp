#include "TArrayTable.h"
#include <algorithm>

TArrayTable::TArrayTable(int size) : TTable() {
  pRecs = new PTTabRecord[size];
  std::fill(pRecs, pRecs + size, nullptr);
  TabSize = size;
  CurrPos = 0;
}

TArrayTable::~TArrayTable() {
  std::for_each(pRecs, pRecs + DataCount, [](PTDatValue ptr) { if (ptr != nullptr) delete ptr; });
  delete[] pRecs;
}

bool TArrayTable::IsFull() const {
  return DataCount >= TabSize;
}

int TArrayTable::GetTabSize() const {
  return TabSize;
}

TKey TArrayTable::GetKey(void) const {
  return GetKey(TDataPos::CURRENT_POS);
}

PTDatValue TArrayTable::GetValuePTR(void) const {
  return GetValuePTR(TDataPos::CURRENT_POS);
}

TKey TArrayTable::GetKey(TDataPos mode) const {
  int pos = -1;
  switch (mode)
  {
  case TDataPos::FIRST_POS:
    pos = 0;
    break;
  case TDataPos::LAST_POS:
    pos = DataCount - 1;
    break;
  case TDataPos::CURRENT_POS:
    pos = CurrPos;
    break;
  }
  return pos != -1 ? pRecs[pos]->Key : "";
}

PTDatValue TArrayTable::GetValuePTR(TDataPos mode) const {
 int pos = -1;
  switch (mode)
  {
  case TDataPos::FIRST_POS:
    pos = 0;
    break;
  case TDataPos::LAST_POS:
    pos = DataCount - 1;
    break;
  case TDataPos::CURRENT_POS:
    pos = CurrPos;
    break;
  }
  return pos != -1 ? pRecs[pos]->pValue : nullptr;
}

void TArrayTable::Reset(void) {
  CurrPos = 0;
}

bool TArrayTable::IsTabEnded(void) const {
  return CurrPos >= DataCount;
}

Data TArrayTable::GoNext(void) {
  if ( !IsTabEnded() )
    CurrPos++;
  else
    SetRetCode(Data::OUT_OF_RANGE);
  return GetRetCode();
}

Data TArrayTable::SetCurrentPos(int pos) {
  if (pos <= 0 && pos > DataCount) {
    SetRetCode(Data::INCORRECT_INCOMING);
  }
  else
    CurrPos = pos;
  return GetRetCode();
}

int TArrayTable::GetCurrentPos(void) const {
  return CurrPos;
}
