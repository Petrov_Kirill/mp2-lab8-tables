// Copyright 2016 Ovcharuk Oleg

#include "TTabRecord.h"

TTabRecord::TTabRecord(TKey k, PTDatValue pVal)
{
    Key = k;
    pValue = pVal;
}

void TTabRecord::SetKey(TKey k)
{
    Key = k;
}

TKey TTabRecord::GetKey(void) const
{
    return Key;
}

void TTabRecord::SetValuePtr(PTDatValue p)
{
    pValue = p;
}

PTDatValue TTabRecord::GetValuePTR(void) const
{
    return pValue;
}

TDatValue * TTabRecord::GetCopy()
{
    TDatValue *tmp = new TTabRecord(Key, pValue);
    return tmp;
}

TTabRecord & TTabRecord::operator=(TTabRecord & tr)
{
    Key = tr.Key;
    pValue = tr.pValue;
    return *this;
}

bool TTabRecord::operator==(const TTabRecord & tr)
{
    return Key == tr.Key;
}

bool TTabRecord::operator<(const TTabRecord & tr)
{
    return Key < tr.Key;
}

bool TTabRecord::operator>(const TTabRecord & tr)
{
    return Key > tr.Key;
}
