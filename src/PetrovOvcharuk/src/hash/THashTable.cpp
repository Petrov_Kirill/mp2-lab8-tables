// Copyright 2016 Petrov Kirill

#include "THashTable.h"


// https://habrahabr.ru/post/219139/
// ������� FAQ6
unsigned long THashTable::HashFunc(const TKey& key)
{
  unsigned int hash = 0;

  for (auto iter = key.begin(); iter != key.end(); ++iter)
  {
    hash += (unsigned char)(*iter);
    hash += (hash << 10);
    hash ^= (hash >> 6);
  }
  hash += (hash << 3);
  hash ^= (hash >> 11);
  hash += (hash << 15);

  return hash;
}
