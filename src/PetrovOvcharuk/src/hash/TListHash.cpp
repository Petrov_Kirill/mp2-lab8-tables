// Copyright 2016 Petrov Kirill

#include "TListHash.h"

#include <algorithm>

TListHash::TListHash(int size) : THashTable() {
  TabSize = size;
  CurrList = 0;
  pList = new PTDataList[TabSize];
  for (int i = 0; i < TabSize; i++)
    pList[i] = new TDatList;
}

TListHash::~TListHash() {
  std::for_each(pList, pList + TabSize, [](PTDataList ptr) {delete ptr; });
  delete[] pList;
}

bool TListHash::IsFull() const {
  return false;
}

TKey TListHash::GetKey(void) const {
 PTTabRecord pRec = static_cast<PTTabRecord> ( pList[CurrList]->GetDatValue() );
 return pRec->GetKey();
}

PTDatValue TListHash::GetValuePTR(void) const {
  PTTabRecord pRec = static_cast<PTTabRecord> (pList[CurrList]->GetDatValue());
  return pRec->GetValuePTR();
}

PTDatValue TListHash::FindRecord(TKey k) {
  CurrList = HashFunc(k) % TabSize;
  PTDataList List = pList[CurrList];
  for (List->Reset(); !List->IsListEnded(); List->GoNext() ) {
    Efficiency++;
    if (static_cast<PTTabRecord>(List->GetDatValue())->GetKey() == k)
      return static_cast<PTTabRecord>(List->GetDatValue())->GetValuePTR();
  }
  SetRetCode(Data::NO_RECORD);
  return nullptr;
}

void TListHash::InsRecord(TKey k, PTDatValue pVal) {
  CurrList = HashFunc(k) % TabSize;
  PTTabRecord pRec = new TTabRecord(k, pVal);
  pList[CurrList]->InsLast(static_cast<PTDatValue>(pRec));
  DataCount++;
}

void TListHash::DelRecord(TKey k) {
  PTDatValue record = FindRecord(k);
  if (record != nullptr) {
    pList[CurrList]->DelCurrent();
    DataCount--;
  }
  else
    SetRetCode(Data::NO_RECORD);
}

void TListHash::Reset(void) {
  CurrList = 0;
  pList[CurrList]->Reset();
}

bool TListHash::IsTabEnded(void) const {
  return CurrList >= TabSize ;
}

Data TListHash::GoNext(void) {
  if (IsTabEnded())
    SetRetCode(Data::FULL_TAB);
  else {
    pList[CurrList]->GoNext();
    if (pList[CurrList]->IsListEnded())
      while (!IsTabEnded() && pList[CurrList]->IsListEnded())
        CurrList++;
  }
  return GetRetCode();
}
