// Copyright 2016 Petrov Kirill

#include "TArrayHash.h"
#include <algorithm>

TArrayHash::TArrayHash(int size) : THashTable() {
  TabSize = size;
  HashStep = 1;
  pRecs = new PTTabRecord[TabSize];
  std::fill(pRecs, pRecs + TabSize, nullptr);
}

TArrayHash::~TArrayHash() {
  std::for_each(pRecs, pRecs + TabSize, [](PTTabRecord ptr) 
  { if (ptr!= nullptr) delete ptr; });
  delete[] pRecs;
}

bool TArrayHash::IsFull() const {
  return false;
}

TKey TArrayHash::GetKey(void) const {
  return pRecs[CurrPos]->GetKey();
}

PTDatValue TArrayHash::GetValuePTR(void) const {
  return pRecs[CurrPos]->GetValuePTR();
}

PTDatValue TArrayHash::FindRecord(TKey k) {
  CurrPos = HashFunc(k) % TabSize;
  for (int i = 0; i < TabSize; i++) {
    Efficiency++;
    if (pRecs[CurrPos] == nullptr) 
      break;
    else if (pRecs[CurrPos]->GetKey() == k)
      return pRecs[CurrPos]->GetValuePTR();
    CurrPos = GetNextPos(CurrPos);
  }
  SetRetCode(Data::NO_RECORD);
  return nullptr;
}

void TArrayHash::InsRecord(TKey k, PTDatValue pVal) {
  CurrPos = HashFunc(k) % TabSize;
  for (int i = 0; i < TabSize; i++) {
    Efficiency++;
    if (pRecs[CurrPos] != nullptr && pRecs[CurrPos]->GetKey() == k)
      return;
    else if (pRecs[CurrPos] == nullptr) {
      pRecs[CurrPos] = new TTabRecord(k, pVal);
      DataCount++;
      return;
    }
    CurrPos = GetNextPos(CurrPos);
  }
  SetRetCode(Data::FULL_TAB);
}

void TArrayHash::DelRecord(TKey k) {
  PTDatValue record = FindRecord(k);
  if (record != nullptr) {
    delete pRecs[CurrPos];
    pRecs[CurrPos] = nullptr;
    DataCount--;
  }
  else
    SetRetCode(Data::NO_RECORD);
}

void TArrayHash::Reset(void) {
  CurrPos = 0;
}

bool TArrayHash::IsTabEnded(void) const {
  return CurrPos >= TabSize;
}

Data TArrayHash::GoNext(void) {
  if (IsTabEnded())
    SetRetCode(Data::FULL_TAB);
  else {
    while (++CurrPos < TabSize)
      if (pRecs[CurrPos] != nullptr)
        break;
  }
  return GetRetCode();
}
