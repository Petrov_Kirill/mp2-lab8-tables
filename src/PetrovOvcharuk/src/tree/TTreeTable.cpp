// Copyright 2016 Ovcharuk Oleg

#include "TTreeTable.h"

void TTreeTable::DeleteTreeTab(PTTreeNode pNode)
{
    if (pNode != nullptr) {
        DeleteTreeTab(pNode->GetLeft());
        DeleteTreeTab(pNode->GetRight());
        delete pNode;
    }
}

bool TTreeTable::IsFull() const // ��������?
{
    return false;
}

PTDatValue TTreeTable::FindRecord(TKey k)
{
    PTTreeNode tmp = pRoot;
    ppRef = &pRoot;
    while (tmp != nullptr) {
        if (tmp->GetKey() == k) break;
        if (tmp->GetKey() < k) ppRef = &tmp->pRight;
        else ppRef = &tmp->pLeft;
        tmp = *ppRef;
    }
    if (tmp == nullptr) {
        SetRetCode(Data::NO_RECORD);
        return nullptr;
    }
    else {
        SetRetCode(Data::OK);
        return tmp->GetValuePTR();
    }
}

void TTreeTable::InsRecord(TKey k, PTDatValue pVal)
{
    if (IsFull()) {
        SetRetCode(Data::FULL_TAB);
    }
    else {
        if (FindRecord(k) != nullptr) {
            SetRetCode(Data::DOUBLE_REC);
        }
        else {
            *ppRef = new TTreeNode(k, pVal);
            DataCount++;
        }
    }
}

void TTreeTable::DelRecord(TKey k)
{
    if (FindRecord(k) == nullptr) {
        SetRetCode(Data::NO_RECORD);
    }
    else {
        PTTreeNode tmp = pRoot;

        while (!St1.empty())
            St1.pop();
        while (tmp->GetKey() != k) {
            St1.push(tmp);
            if (tmp->GetKey() < k)
                tmp = tmp->GetRight();
            else
                tmp = tmp->GetLeft();
        }
        // �������� �����
        if ((tmp->pLeft == nullptr) && (tmp->pRight == nullptr)) {
            if (!St1.empty()) {
                PTTreeNode prev = St1.top();
                if (prev != nullptr) {
                    if (prev->GetRight() == tmp)
                        prev->pRight = nullptr;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = nullptr;
                }
            }
            else {
                pRoot = nullptr;
            }
            delete tmp;
            DataCount--;
        }
        // �������� ����� � ����� �������� (������)
        else if (tmp->pLeft == nullptr) {
            if (!St1.empty()) {
                PTTreeNode prev = St1.top();
                if (prev != nullptr) {
                    if (prev->GetRight() == tmp)
                        prev->pRight = tmp->pRight;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = tmp->pRight;
                }
            }
            else {
                pRoot = tmp->GetRight();
            }
            delete tmp;
            DataCount--;
        }
        // �������� ����� � ����� �������� (�����)
        else if (tmp->pRight == nullptr) {
            if (!St1.empty()) {
                PTTreeNode prev = St1.top();
                if (prev != nullptr) {
                    if (prev->GetRight() == tmp)
                        prev->pRight = tmp->pLeft;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = tmp->pLeft;
                }
            }
            else {
                pRoot = tmp->GetLeft();
            }
            delete tmp;
            DataCount--;
        }
        // �������� ����� � ����� ���������
        else {
            PTTreeNode down_left = tmp->GetRight();
            while (down_left->GetLeft() != nullptr)
                down_left = down_left->pLeft;
            down_left->pLeft = tmp->GetLeft();

            if (!St1.empty()) {
                PTTreeNode prev = St1.top();
                if (prev != nullptr) {
                    if (prev->GetRight() == tmp)
                        prev->pRight = tmp->pRight;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = tmp->pRight;
                }
            }
            else {
                pRoot = tmp->GetRight();
            }

            delete tmp;
            DataCount--;
        }
        
    }
}

TKey TTreeTable::GetKey(void) const
{
    return (pCurrent == nullptr) ? "" : pCurrent->GetKey();
}

PTDatValue TTreeTable::GetValuePTR(void) const
{
    return (pCurrent == nullptr) ? nullptr : pCurrent->GetValuePTR();
}

void TTreeTable::Reset(void)
{
    PTTreeNode tmp = pCurrent = pRoot;
    CurrPos = 0;
    while (tmp != nullptr) {
        St.push(tmp);
        pCurrent = tmp;
        tmp = tmp->GetLeft();
    }
    SetRetCode(Data::OK);
}

bool TTreeTable::IsTabEnded(void) const
{
    return (CurrPos >= DataCount);
}

Data TTreeTable::GoNext(void)
{
    CurrPos++;
    if (!IsTabEnded() && (pCurrent != nullptr)) {
        PTTreeNode pNode = pCurrent = pCurrent->GetRight();
        St.pop();
        while (pNode != nullptr) {
            St.push(pNode);
            pCurrent = pNode;
            pNode = pNode->GetLeft();
        }
        if ((pCurrent == nullptr) && !St.empty())
            pCurrent = St.top();
        
    }
    else SetRetCode(Data::OUT_OF_RANGE);
    return GetRetCode();
}
