// Copyright 2016 Ovcharuk Oleg

#include "TBalanceNode.h"

TDatValue * TBalanceNode::GetCopy()
{
    TBalanceNode *tmp = new TBalanceNode(Key, pValue, nullptr, nullptr, Bal::BalOK);
    return tmp;
}

Bal TBalanceNode::GetBalance(void) const
{
    return Balance;
}

void TBalanceNode::SetBalance(Bal bal)
{
    Balance = bal;
}
