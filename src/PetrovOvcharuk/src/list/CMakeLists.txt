set(target "list")

file(GLOB hrcs ../../${INCLUDE}/*h ../../${INCLUDE}/${target}/*h)
file(GLOB srcs "*.cpp")

# Add library this
add_library(${target} STATIC ${SOURCES} ${hrcs}  ${srcs} )