// Copyright 2016 Ovcharuk Oleg

#include "TTabRecord.h"
#include <gtest.h>

// Testing type (string)
typedef struct TString : TDatValue
{
    std::string Value;
    TString(std::string val = "") { Value = val; }
    virtual TDatValue * GetCopy() { TDatValue *temp = new TString(Value); return temp; }
} *PTString;

TEST(TTabRecord, can_correct_get_key) {
    // Arrange
    PTString Val = new TString("na samom dele ya lublu aisd");
    TTabRecord Tab("qwe", Val);

    // Act & Assert
    EXPECT_EQ("qwe", Tab.GetKey());
}

TEST(TTabRecord, can_correct_set_key) {
    // Arrange
    PTString Val = new TString("yo");
    TTabRecord Tab("qwe", Val);

    // Act
    Tab.SetKey("qwe1");

    // Assert
    EXPECT_EQ("qwe1", Tab.GetKey());
}

TEST(TTabRecord, can_correct_get_pValue) {
    // Arrange
    PTString Val = new TString("idk how it works");
    TTabRecord Tab("wow", Val);

    // Act & Assert
    EXPECT_EQ(Val, Tab.GetValuePTR());
}

TEST(TTabRecord, can_correct_set_pValue) {
    // Arrange
    PTString Val = new TString("bilo");
    TTabRecord Tab("tut dolzhna bit' pashalochka", Val);

    // Act
    PTString Val2 = new TString("stalo");
    Tab.SetValuePtr(Val2);

    // Assert
    EXPECT_EQ(Val2, Tab.GetValuePTR());
}

TEST(TTabRecord, can_create_copy) {
    // Arrange
    PTString Val = new TString("ya ne drish =(");
    TTabRecord Tab("qwe", Val);

    // Act
    PTTabRecord Tab2 = (PTTabRecord)Tab.GetCopy();

    // Assert
    EXPECT_EQ(Tab2->GetKey(), Tab.GetKey());
    EXPECT_EQ(Tab2->GetValuePTR(), Tab.GetValuePTR());
}

TEST(TTabRecord, can_assign_tab) {
    // Arrange
    PTString Val = new TString("I Love Testing");
    TTabRecord Tab1("wow", Val);
    TTabRecord Tab2("so much wow", Val);

    // Act
    Tab1 = Tab2;

    // Assert
    EXPECT_EQ(Tab1.GetKey(), Tab2.GetKey());
}

TEST(TTabRecord, can_compare_tabs) {
    // Arrange
    PTString Val1 = new TString("otlichnik");
    PTString Val2 = new TString("dvoechnik");
    TTabRecord Tab1("Ovcharuk", Val1);
    TTabRecord Tab2("Ovcharuk", Val2);
    TTabRecord Tab3("Petrov", Val2);
    TTabRecord Tab4("A - pervaya bukva", Val1);

    // Act & Assert
    EXPECT_TRUE(Tab1 == Tab2);
    EXPECT_TRUE(Tab1 < Tab3);
    EXPECT_TRUE(Tab3 > Tab4);
}
