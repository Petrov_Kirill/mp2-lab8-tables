// Copyright 2016 Petrov Kirill

#include <gtest.h>

#include <set>

#include "TArrayHash.h"


// Testing type (string)
typedef struct TString : public TDatValue, public std::string {
  TString(const std::string& str) : std::string(str){};
  TString(const TString& str) : std::string(str) {};
  virtual PTDatValue GetCopy() { return new TString(*this); }  
}* PTString;    // Initialization  

class TestTArrayHash : public ::testing::Test {
protected:
  // Arrange
  static const int N = 10;

  PTString values[N] = {
    new TString("5"),
    new TString("2"),
    new TString("6"),
    new TString("2"),
    new TString("3"),
    new TString("3"),
    new TString("4"),
    new TString("5"),
    new TString("1"),
    new TString("0"),
  };

  std::string keys[N] = {
    "Petrov",
    "Ivanov",
    "Sidorov",
    "Sidok",
    "Trov",
    "Petr",
    "Fera",
    "Ovsharuk",
    "Lada",
    "Sharuk"
  };
};  

TEST_F(TestTArrayHash, can_insert_once_record) {
  // Arrange
  TArrayHash table (N);

  // Act
  table.InsRecord(keys[0], values[0]);

  // Assert
  EXPECT_EQ(table.GetValuePTR(), values[0]);
  EXPECT_EQ(table.GetKey(), keys[0]);
}

TEST_F(TestTArrayHash, can_insert_ten_record) {
  // Arrange
  TArrayHash table(N);

  // Act
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], values[i]);

  // Assert
  EXPECT_EQ(table.GetDataCount(), N);
}

TEST_F(TestTArrayHash, cant_insert_record_due_to_overflow) {
  // Arrange
  TArrayHash table(N);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], values[i]);

  // Act
  table.InsRecord("Rack", new TString("2"));

  // Assert
  EXPECT_EQ(Data::FULL_TAB, table.GetRetCode());
}

TEST_F(TestTArrayHash, can_insert_record_due_to_conflict) {
  // Arrange
  TArrayHash table(N);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[0], values[0]);

  // Act & Assert
  EXPECT_EQ(Data::OK, table.GetRetCode());
}

TEST_F(TestTArrayHash, can_find_record) {
  // Arrange
  TArrayHash table(N);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], values[i]);

  // Act
  PTDatValue result = table.FindRecord("Sharuk");
  
  // Assert
  PTDatValue expected_value = values[N-1];
  EXPECT_EQ(expected_value, result);
}

TEST_F(TestTArrayHash, can_find_record_witch_not_full_table) {
  // Arrange
  const int M = 5;
  TArrayHash table(M);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], values[i]);

  // Act
  PTDatValue result = table.FindRecord("Petrov");
  
  // Assert
  PTDatValue expected_value = values[0];
  EXPECT_EQ(expected_value, result);
}

TEST_F(TestTArrayHash, cant_find_record_witch_not_full_table) {
  // Arrange
  const int M = 5;
  TArrayHash table(N);
  for (int i = 0; i < M; i++)
    table.InsRecord(keys[i], values[i]);

  // Act
  table.FindRecord("Petov");
  
  // Assert
  EXPECT_EQ(Data::NO_RECORD, table.GetRetCode());
}

TEST_F(TestTArrayHash, cant_find_record) {
  // Arrange
  TArrayHash table(N);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], values[i]);

  // Act
  table.FindRecord("Shak");
  
  // Assert
  EXPECT_EQ(Data::NO_RECORD, table.GetRetCode());
}

TEST_F(TestTArrayHash, can_delete_record) {
  // Arrange
  TArrayHash table(N);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], values[i]);

  // Act
  table.DelRecord("Petrov");

  // Assert
  EXPECT_EQ(table.GetDataCount(), N-1);
}

TEST_F(TestTArrayHash, cant_find_delete_record) {
  // Arrange
  TArrayHash table(N);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], values[i]);

  // Act
  table.DelRecord("Petrov");

  // Assert
  EXPECT_EQ(nullptr, table.FindRecord("Petrov") );
}

TEST_F(TestTArrayHash, can_insert_after_delete_record) {
  // Arrange
  TArrayHash table(N);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], values[i]);

  // Act
  table.DelRecord("Petr");
  PTString expected_ptr = new TString("2");
  table.InsRecord("Petr1", expected_ptr);

  // Assert
  EXPECT_EQ(expected_ptr, table.FindRecord("Petr1")) ;
}

TEST_F(TestTArrayHash, can_make_navigation) {
  // Arrange
  TArrayHash table(N);
  std::set<std::string> expected_set;

  for (int i = 0; i < N; i++) {
    table.InsRecord(keys[i], values[i]);
    expected_set.insert(keys[i]);
  }

  // Act
  std::set<std::string> result_set;
  for (table.Reset(); !table.IsTabEnded(); table.GoNext())
    result_set.insert(table.GetKey());

  // Assert
  EXPECT_EQ(expected_set, result_set);
}

TEST_F(TestTArrayHash, cant_make_navigation) {
  // Arrange
  TArrayHash table(N);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], values[i]);

  // Act
  for (table.Reset(); !table.IsTabEnded(); table.GoNext())
    ;

  // Assert
  EXPECT_EQ(Data::FULL_TAB, table.GoNext());
}