// Copyright 2016 Ovcharuk Oleg

#include <gtest.h>
#include "TTreeTable.h"
#include <iostream>

// Testing type (string)
typedef struct TString : TDatValue
{
    std::string Value;
    TString(std::string val = "") { Value = val; }
    virtual TDatValue * GetCopy() { TDatValue *temp = new TString(Value); return temp; }
} *PTString;

// Initialization
class TestTTreeTable : public ::testing::Test {
protected:
    virtual void SetUp() {
        // Testing ten elems 
        for (int i = 0; i < N; i++) {
            arrVal[i] = new TString();
            arrVal[i]->Value = 'a' + i;
        }

        // Create ten keys
        for (int i = 0; i < N; i++)
            keys[i] = std::to_string(i);
    }
    // Arrange
    static const int N = 10;

    // value
    PTString arrVal[N];

    // key
    TKey keys[N];
};

TEST_F(TestTTreeTable, can_insert_records_with_correct_order) {
    // Arrange
    TTreeTable Tab;

    //Act
    Tab.InsRecord(keys[4], arrVal[4]);
    Tab.InsRecord(keys[2], arrVal[2]);
    Tab.InsRecord(keys[5], arrVal[5]);
    Tab.InsRecord(keys[1], arrVal[1]);
    Tab.InsRecord(keys[3], arrVal[3]);
    Tab.Reset();

    // Assert
    for (int i = 1; i < 6; i++) {
        EXPECT_EQ(Tab.GetKey(), std::to_string(i));
        EXPECT_EQ(Tab.GetValuePTR(), arrVal[i]);
        Tab.GoNext();
    }
}

TEST_F(TestTTreeTable, cant_delete_nonexistent_record) {
    // Arrange
    TTreeTable Tab;
    Tab.InsRecord(keys[3], arrVal[3]);
    Tab.InsRecord(keys[1], arrVal[1]);
    Tab.InsRecord(keys[4], arrVal[4]);
    Tab.InsRecord(keys[9], arrVal[9]);
    Tab.InsRecord(keys[0], arrVal[0]);
    Tab.Reset();

    // Act
    Tab.DelRecord("6");

    // Assert
    EXPECT_EQ(Tab.GetRetCode(), Data::NO_RECORD);
}

TEST_F(TestTTreeTable, can_delete_record_with_no_child1) {
    // Arrange
    TTreeTable Tab;
    Tab.InsRecord(keys[3], arrVal[3]);
    Tab.InsRecord(keys[1], arrVal[1]);
    Tab.InsRecord(keys[4], arrVal[4]);
    Tab.InsRecord(keys[9], arrVal[9]);
    Tab.InsRecord(keys[0], arrVal[0]);
    Tab.Reset();

    // Act
    Tab.DelRecord("0");

    // Assert
    EXPECT_EQ(Tab.FindRecord("0"), nullptr);
}

TEST_F(TestTTreeTable, can_delete_record_with_no_child2) {
    // Arrange
    TTreeTable Tab;
    Tab.InsRecord(keys[9], arrVal[9]);
    Tab.Reset();

    // Act
    Tab.DelRecord("9");

    // Assert
    EXPECT_EQ(Tab.FindRecord("9"), nullptr);
}

TEST_F(TestTTreeTable, can_delete_record_with_one_child_left) {
    // Arrange
    TTreeTable Tab;
    Tab.InsRecord(keys[3], arrVal[3]);
    Tab.InsRecord(keys[1], arrVal[1]);
    Tab.InsRecord(keys[4], arrVal[4]);
    Tab.InsRecord(keys[9], arrVal[9]);
    Tab.InsRecord(keys[0], arrVal[0]);
    Tab.Reset();

    // Act
    Tab.DelRecord("1");

    // Assert
    EXPECT_EQ(Tab.FindRecord("1"), nullptr);
}

TEST_F(TestTTreeTable, can_delete_record_with_one_child_right1) {
    // Arrange
    TTreeTable Tab;
    Tab.InsRecord(keys[2], arrVal[2]);
    Tab.InsRecord(keys[1], arrVal[1]);
    Tab.InsRecord(keys[5], arrVal[5]);
    Tab.InsRecord(keys[4], arrVal[4]);
    Tab.InsRecord(keys[9], arrVal[9]);
    Tab.InsRecord(keys[0], arrVal[0]);
    Tab.InsRecord(keys[3], arrVal[3]);
    Tab.Reset();

    // Act
    Tab.DelRecord("4");

    // Assert
    EXPECT_EQ(Tab.FindRecord("4"), nullptr);
}

TEST_F(TestTTreeTable, can_delete_record_with_one_child_right2) {
    // Arrange
    TTreeTable Tab;
    Tab.InsRecord(keys[2], arrVal[2]);
    Tab.InsRecord(keys[3], arrVal[3]);
    Tab.Reset();

    // Act
    Tab.DelRecord("2");

    // Assert
    EXPECT_EQ(Tab.FindRecord("2"), nullptr);
}

TEST_F(TestTTreeTable, can_delete_record_with_both_children1) {
    // Arrange
    TTreeTable Tab;
    Tab.InsRecord(keys[2], arrVal[2]);
    Tab.InsRecord(keys[3], arrVal[3]);
    Tab.InsRecord(keys[1], arrVal[1]);
    Tab.Reset();

    // Act
    Tab.DelRecord("2");

    // Assert
    EXPECT_EQ(Tab.FindRecord("2"), nullptr);
}

TEST_F(TestTTreeTable, can_delete_record_with_both_children2) {
    // Arrange
    TTreeTable Tab;
    Tab.InsRecord(keys[2], arrVal[2]);
    Tab.InsRecord(keys[1], arrVal[1]);
    Tab.InsRecord(keys[5], arrVal[5]);
    Tab.InsRecord(keys[4], arrVal[4]);
    Tab.InsRecord(keys[9], arrVal[9]);
    Tab.InsRecord(keys[0], arrVal[0]);
    Tab.InsRecord(keys[3], arrVal[3]);

    // Act
    Tab.DelRecord("5");

    // Assert
    EXPECT_EQ(Tab.FindRecord("5"), nullptr);
}

TEST_F(TestTTreeTable, can_find_record) {
    // Arrange
    TTreeTable Tab;
    Tab.InsRecord(keys[3], arrVal[3]);
    Tab.InsRecord(keys[1], arrVal[1]);
    Tab.InsRecord(keys[4], arrVal[4]);
    Tab.InsRecord(keys[9], arrVal[9]);
    Tab.InsRecord(keys[0], arrVal[0]);
    Tab.Reset();

    // Act & Assert
    EXPECT_EQ(Tab.FindRecord("9"), arrVal[9]);
    EXPECT_EQ(Tab.GetRetCode(), Data::OK);
}

TEST_F(TestTTreeTable, cant_find_record) {
    // Arrange
    TTreeTable Tab;
    Tab.InsRecord(keys[5], arrVal[3]);
    Tab.InsRecord(keys[3], arrVal[1]);
    Tab.InsRecord(keys[8], arrVal[4]);
    Tab.InsRecord(keys[9], arrVal[9]);
    Tab.InsRecord(keys[1], arrVal[0]);
    Tab.InsRecord(keys[2], arrVal[0]);
    Tab.Reset();

    // Act & Assert
    EXPECT_EQ(Tab.FindRecord("4"), nullptr);
    EXPECT_EQ(Tab.GetRetCode(), Data::NO_RECORD);
}

TEST_F(TestTTreeTable, cant_insert_record_with_the_same_key) {
    // Arrange
    TTreeTable Tab;

    // Act
    Tab.InsRecord(keys[1], arrVal[1]);
    Tab.InsRecord(keys[1], arrVal[1]);

    // Assert
    EXPECT_EQ(Tab.GetRetCode(), Data::DOUBLE_REC);
}

TEST_F(TestTTreeTable, cant_go_out_of_range) {
    // Arrange
    TTreeTable Tab;

    // Act
    Tab.InsRecord(keys[1], arrVal[1]);
    Tab.InsRecord(keys[2], arrVal[2]);
    Tab.InsRecord(keys[8], arrVal[4]);
    Tab.InsRecord(keys[9], arrVal[9]);
    Tab.InsRecord(keys[1], arrVal[0]);
    Tab.InsRecord(keys[2], arrVal[0]);
    for (Tab.Reset(); !Tab.IsTabEnded(); Tab.GoNext())
        ;

    // Assert
    EXPECT_EQ(Data::OUT_OF_RANGE, Tab.GoNext());
}