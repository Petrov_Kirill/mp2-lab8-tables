// Copyright 2016 Ovcharuk Oleg

#include <gtest.h>
#include "TSortTable.h"

// Testing type (string)
typedef struct TString : TDatValue {
    std::string Value;

    bool operator== (const TString& out) const { return Value.compare(out.Value) == 0; }
    TString(std::string val = "") { Value = val; }
    virtual TDatValue * GetCopy() { TDatValue *temp = new TString(Value); return temp; }
}*PTString;

// Initialization
class TestTSortTable : public ::testing::Test {
protected:
    virtual void SetUp() {
        // Testing ten elems 
        for (int i = 0; i < N; i++) {
            arrVal[i] = new TString();
            arrVal[i]->Value = 'a' + i;
        }

        // Create ten keys
        for (int i = 0; i < N; i++)
            keys[i] = std::to_string(i);
    }
    // Arrange
    static const int N = 10;

    // value
    PTString arrVal[N];

    // key
    std::string keys[N];
};

TEST_F(TestTSortTable, can_correct_setNget_sort_method) {
    // Arrange
    TSortTable Tab;

    // Act & Assert
    Tab.SetSortMethod(TSortMethod::INSERT_SORT);
    EXPECT_EQ(Tab.GetSortMethod(), TSortMethod::INSERT_SORT);
    Tab.SetSortMethod(TSortMethod::QUICK_SORT);
    EXPECT_EQ(Tab.GetSortMethod(), TSortMethod::QUICK_SORT);
    Tab.SetSortMethod(TSortMethod::MERGE_SORT);
    EXPECT_EQ(Tab.GetSortMethod(), TSortMethod::MERGE_SORT);
}

TEST_F(TestTSortTable, can_add_one_record) {
    // Arrange
    TSortTable Tab;
    
    // Act
    Tab.InsRecord(keys[1], arrVal[1]);
    Tab.Reset();

    // Assert
    EXPECT_EQ(Tab.GetKey(), keys[1]);
}

TEST_F(TestTSortTable, can_add_several_records_Quick) {
    // Arrange
    TSortTable Tab;
    Tab.SetSortMethod(TSortMethod::QUICK_SORT);

    // Act
    for (int j = 9; j >= 0; j = j - 2) {
        Tab.InsRecord(keys[j], arrVal[j]);
    }
    for (int j = 8; j >= 0; j = j - 2) {
        Tab.InsRecord(keys[j], arrVal[j]);
    }

    // Assert
    int i = 0;
    for (Tab.Reset(); !Tab.IsTabEnded(); Tab.GoNext()) {
        EXPECT_EQ(Tab.FindRecord(std::to_string(i)), arrVal[i]);
        i++;
    }
}

TEST_F(TestTSortTable, can_add_several_records_Insert) {
    // Arrange
    TSortTable Tab;
    Tab.SetSortMethod(TSortMethod::INSERT_SORT);

    // Act
    for (int j = 9; j >= 0; j = j - 2) {
        Tab.InsRecord(keys[j], arrVal[j]);
    }
    for (int j = 8; j >= 0; j = j - 2) {
        Tab.InsRecord(keys[j], arrVal[j]);
    }

    // Assert
    int i = 0;
    for (Tab.Reset(); !Tab.IsTabEnded(); Tab.GoNext()) {
        EXPECT_EQ(Tab.FindRecord(std::to_string(i)), arrVal[i]);
        i++;
    }
}

TEST_F(TestTSortTable, can_add_several_records_Merge) {
    // Arrange
    TSortTable Tab;
    Tab.SetSortMethod(TSortMethod::MERGE_SORT);

    // Act
    for (int j = 9; j >= 0; j = j-2) {
        Tab.InsRecord(keys[j], arrVal[j]);
    }
    for (int j = 8; j >= 0; j = j - 2) {
        Tab.InsRecord(keys[j], arrVal[j]);
    }

    // Assert
    int i = 0;
    for (Tab.Reset(); !Tab.IsTabEnded(); Tab.GoNext()) {
        EXPECT_EQ(Tab.FindRecord(std::to_string(i)), arrVal[i]);
        i++;
    }
}

TEST_F(TestTSortTable, can_insert_once_record) {
    // Arrange
    TSortTable table(N);

    // Act
    table.InsRecord(keys[0], arrVal[0]);

    // Assert
    EXPECT_EQ(table.GetValuePTR(TDataPos::FIRST_POS), arrVal[0]);
    EXPECT_EQ(table.GetKey(TDataPos::FIRST_POS), keys[0]);
}

TEST_F(TestTSortTable, can_insert_ten_record) {
    // Arrange
    TSortTable table(N);

    // Act
    for (int i = 0; i < N; i++)
        table.InsRecord(keys[i], arrVal[i]);

    // Assert
    int current = 0;
    for (table.Reset(); table.IsTabEnded(); table.GoNext()) {
        EXPECT_EQ(table.GetValuePTR(TDataPos::FIRST_POS), arrVal[current]);
        EXPECT_EQ(table.GetKey(TDataPos::FIRST_POS), keys[current]);
        current++;
    }
}

TEST_F(TestTSortTable, can_find_record) {
    // Arrange
    TSortTable table(N);

    for (int i = 0; i < N; i++)
        table.InsRecord(keys[i], arrVal[i]);

    // Act
    PTDatValue result = table.FindRecord("2");

    // Assert
    PTDatValue expected_value = arrVal[2];
    EXPECT_EQ(expected_value, result);
}

TEST_F(TestTSortTable, can_delete_record) {
    // Arrange
    TSortTable table(N);

    for (int i = 0; i < N; i++)
        table.InsRecord(keys[i], arrVal[i]);

    // Act
    table.DelRecord("2");

    // Assert
    PTDatValue expected_value = table.FindRecord("2");
    EXPECT_EQ(table.FindRecord("2"), nullptr);
}

TEST_F(TestTSortTable, can_delete_ten_records) {
    // Arrange
    TSortTable table(N);

    for (int i = 0; i < N; i++)
        table.InsRecord(keys[i], arrVal[i]);

    // Act
    for (int i = N - 1; i >= 0; i--)
        table.DelRecord(keys[i]);

    // Assert
    const int expected_count = 0;
    EXPECT_EQ(expected_count, table.GetDataCount());
}

TEST_F(TestTSortTable, cant_insert_record) {
    // Arrange
    TSortTable table(N - 1);
    for (int i = 0; i < N - 1; i++)
        table.InsRecord(keys[i], arrVal[i]);

    // Act
    table.InsRecord(keys[N - 1], arrVal[N - 1]);

    // Assert
    EXPECT_EQ(Data::FULL_TAB, table.GetRetCode());
}

TEST_F(TestTSortTable, cant_find_record) {
    // Arrange
    TSortTable table(N);
    for (int i = 0; i < N; i++)
        table.InsRecord(keys[i], arrVal[i]);

    // Act
    table.FindRecord("a");

    // Assert
    EXPECT_EQ(Data::NO_RECORD, table.GetRetCode());
}

TEST_F(TestTSortTable, cant_delete_record) {
    // Arrange
    TSortTable table(N);
    for (int i = 0; i < N; i++)
        table.InsRecord(keys[i], arrVal[i]);

    // Act
    table.DelRecord("a");

    // Assert
    EXPECT_EQ(Data::NO_RECORD, table.GetRetCode());
}

TEST_F(TestTSortTable, can_get_last_value) {
    // Arrange
    TSortTable table(N);

    for (int i = 0; i < N; i++)
        table.InsRecord(keys[i], arrVal[i]);

    // Act
    PTDatValue result = table.GetValuePTR(TDataPos::LAST_POS);

    // Assert
    PTDatValue expected_value = arrVal[N - 1];
    EXPECT_EQ(expected_value, result);
}

TEST_F(TestTSortTable, cant_get_error_record) {
    // Arrange
    TSortTable table(N);
    for (int i = 0; i < N; i++)
        table.InsRecord(keys[i], arrVal[i]);

    // Act
    table.FindRecord("1");

    // Assert
    EXPECT_EQ(Data::OK, table.GetRetCode());
}

TEST_F(TestTSortTable, cant_make_navigation) {
    // Arrange
    TSortTable table(1);
    PTDatValue ptrValue = new TString("4");
    table.InsRecord("Red", ptrValue);

    // Act
    for (table.Reset(); !table.IsTabEnded(); table.GoNext())
        ;

    // Assert
    EXPECT_EQ(Data::OUT_OF_RANGE, table.GoNext());
}

TEST_F(TestTSortTable, can_make_navigation) {
    // Arrange
    TSortTable table(N);
    std::set<std::string> expected_set;

    for (int i = 0; i < N; i++) {
        table.InsRecord(keys[i], arrVal[i]);
        expected_set.insert(keys[i]);
    }

    // Act
    std::set<std::string> result_set;
    for (table.Reset(); !table.IsTabEnded(); table.GoNext())
        result_set.insert(table.GetKey());

    // Assert
    EXPECT_EQ(expected_set, result_set);
}