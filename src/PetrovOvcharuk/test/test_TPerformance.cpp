// Copyright 2016 Petrov Kirill

#include <gtest.h>

#include <iostream>
#include <locale>
#include <set>
#include <sstream>

#include "TPerformance.h"

#include "TScanTable.h"
#include "TArrayHash.h"
#include "TListHash.h"
#include "TBalanceTree.h"
#include "TTreeTable.h"

using std::ifstream;
using std::set;

TEST(TPerformance, can_create_table_from_TListHash) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TListHash> table(group);
  group.close();
 
  // Act
  set<TKey> result;
  for (table.Reset(); !table.IsTabEnded(); table.GoNext())
    result.insert(table.GetKey());

  // Assert
  TKey buff;
  set<TKey> expected_set;
  group.open("test.txt");
  while (group >> buff)
    if ( !atoi(buff.c_str()) )
      expected_set.insert(buff);
  group.close();
  EXPECT_EQ(expected_set, result);
}

TEST(TPerformance, can_create_table_from_TArrayHash) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TArrayHash> table(group);
  group.close();

  // Act
  set<TKey> result;
  for (table.Reset(); !table.IsTabEnded(); table.GoNext())
    result.insert(table.GetKey());

  // Assert
  TKey buff;
  set<TKey> expected_set;
  group.open("test.txt");
  while (group >> buff)
    if (!atoi(buff.c_str()))
      expected_set.insert(buff);
  group.close();
  EXPECT_EQ(expected_set, result);
}


TEST(TPerformance, can_create_table_from_TTreeTable) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TTreeTable> table(group);
  group.close();
  // Act
  set<TKey> result;
  for (table.Reset(); !table.IsTabEnded(); table.GoNext())
    result.insert(table.GetKey());

  // Assert
  TKey buff;
  set<TKey> expected_set;
  group.open("test.txt");
  while (group >> buff)
    if (!atoi(buff.c_str()))
      expected_set.insert(buff);
  group.close();
  EXPECT_EQ(expected_set, result);
}

TEST(TPerformance, can_create_table_from_TBalanceTree) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TBalanceTree> table(group);
  group.close();
  // Act
  set<TKey> result;
  for (table.Reset(); !table.IsTabEnded(); table.GoNext())
    result.insert(table.GetKey());

  // Assert
  TKey buff;
  set<TKey> expected_set;
  group.open("test.txt");
  while (group >> buff)
    if (!atoi(buff.c_str()))
      expected_set.insert(buff);
  group.close();
  EXPECT_EQ(expected_set, result);
}


TEST(TPerformance, can_create_table_from_TScanTable) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TScanTable> table(group);
  group.close();

  // Act
  set<TKey> result;
  for (table.Reset(); !table.IsTabEnded(); table.GoNext())
    result.insert(table.GetKey());

  // Assert
  TKey buff;
  set<TKey> expected_set;
  group.open("test.txt");
  while (group >> buff)
    if (!atoi(buff.c_str()))
      expected_set.insert(buff);
  group.close();
  EXPECT_EQ(expected_set, result);
}

TEST(TPerformance, can_find_average_rating_student) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TListHash> table(group);
  group.close();

  // Act
  const TKey student = "BoyGeorge";
  const double result = table.averageStudent(student);

  // Assert
  const double expected_value = 3.5;
  EXPECT_DOUBLE_EQ(expected_value, result);
}

TEST(TPerformance, can_get_velue_evalution_student) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TListHash> table(group);
  group.close();

  // Act
  PTEvaluation ptr = static_cast<PTEvaluation>(table.FindRecord("EnochPowell"));

  // Assert
  
  const TEvaluation expected_value ({ A, F, B, AA, D, F });
  EXPECT_EQ(expected_value, *ptr);
}

TEST(TPerformance, can_find_average_rating_group) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TBalanceTree> table(group);
  group.close();

  // Act
  const double result = table.averageAllSubjectsGroup();

  // Assert
  const double expected_value = 3.5454545454545463;
  EXPECT_DOUBLE_EQ(expected_value, result);
}

TEST(TPerformance, can_calc_average_rating_group_in_subjects) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TBalanceTree> table(group);
  group.close();

  // Act
  const double result = table.averageSubjectsGroup("Calculus");

  // Assert
  const double expected_value = 3.590909090909090908;
  EXPECT_DOUBLE_EQ(expected_value, result);
}

TEST(TPerformance, can_print_subjects) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TBalanceTree> table(group);
  group.close();

  // Act
  std::ostringstream stream;
  table.printSubjects(stream);

  // Assert
  const string expected_string = "Algebra Archael Art Biology Botany Calculus ";
  EXPECT_EQ(expected_string, stream.str());
}

TEST(TPerformance, can_print) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TBalanceTree> table(group);
  group.close();

  // Act
  table.printGroup(std::cout);
}

TEST(TPerformance, can_find_eval_student) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TListHash> table(group);
  group.close();

  // Act
  Evaluation result = table.findStudentEvalution("BoyGeorge", "Biology");

  // Assert
  EXPECT_EQ(A, result);
}

TEST(TPerformance, can_search_standouts_student) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TArrayHash> table(group);
  group.close();

  // Act
  int result = table.searchStandouts("Calculus");

    // Assert
  const int expected_count = 8;
  EXPECT_EQ(expected_count, result);
}

TEST(TPerformance, can_search_fellows_student) {
  // Arrange
  ifstream group;
  group.open("test.txt");
  TPeformance<TArrayHash> table(group);
  group.close();

  // Act
  int result = table.searchFellows();

  // Assert
  const int expected_count = 0;
  EXPECT_EQ(expected_count, result);
}
