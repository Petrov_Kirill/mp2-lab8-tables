// Copyright 2016 Petrov Kirill

#include <gtest.h>

#include <set>
#include <string>

#include "TScanTable.h"

// Testing type (string)
typedef struct TString : TDatValue {
  std::string Value;

  bool operator== (const TString& out) const { return Value.compare(out.Value) == 0; }
  TString(std::string val = "") { Value = val; }
  virtual TDatValue * GetCopy() { TDatValue *temp = new TString(Value); return temp; }
}* PTString;

// Initialization
class TestTScanTable : public ::testing::Test {
protected:
  virtual void SetUp() {
    // Testing ten elems 
    for (int i = 0; i < N; i++) {
      arrVal[i] = new TString();
      arrVal[i]->Value = 'a' + i;
    }

    // Create ten keys
    for (int i = 0; i < N; i++)
      keys[i] = std::to_string(i);
  }
  // Arrange
  static const int N = 10;

  // value
  PTString arrVal[N];

  // key
  std::string keys[N];
};

TEST_F(TestTScanTable, can_insert_once_record) {
  // Arrange
  TScanTable table(N);

  // Act
  table.InsRecord(keys[0], arrVal[0]);

  // Assert
  EXPECT_EQ(table.GetValuePTR(TDataPos::FIRST_POS), arrVal[0]);
  EXPECT_EQ(table.GetKey(TDataPos::FIRST_POS), keys[0]);
}

TEST_F(TestTScanTable, can_insert_ten_record) {
  // Arrange
  TScanTable table(N);

  // Act
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], arrVal[i]);

  // Assert
  int current = 0;
  for ( table.Reset(); table.IsTabEnded(); table.GoNext() ) {
    EXPECT_EQ(table.GetValuePTR(TDataPos::FIRST_POS), arrVal[current]);
    EXPECT_EQ(table.GetKey(TDataPos::FIRST_POS), keys[current]);
    current++;
  }
}

TEST_F(TestTScanTable, can_find_record) {
  // Arrange
  TScanTable table(N);

  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], arrVal[i]);

  // Act
  PTDatValue result = table.FindRecord("2");

  // Assert
  PTDatValue expected_value = arrVal[2];
  EXPECT_EQ(expected_value, result);
}

TEST_F(TestTScanTable, can_delete_record) {
  // Arrange
  TScanTable table(N);

  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], arrVal[i]);

  // Act
  table.DelRecord("2");

  // Assert
  PTDatValue expected_value = table.FindRecord("2");
  EXPECT_EQ(expected_value, nullptr);
}

TEST_F(TestTScanTable, can_delete_ten_records) {
  // Arrange
  TScanTable table(N);

  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], arrVal[i]);

  // Act
  for (int i = N - 1; i >= 0; i--)
    table.DelRecord(keys[i]);

  // Assert
  const int expected_count = 0;
  EXPECT_EQ(expected_count, table.GetDataCount() );
}

TEST_F(TestTScanTable, cant_insert_record) {
  // Arrange
  TScanTable table(N-1);
  for (int i = 0; i < N-1; i++)
    table.InsRecord(keys[i], arrVal[i]);

  // Act
  table.InsRecord(keys[N - 1], arrVal[N - 1]);

  // Assert
  EXPECT_EQ(Data::OUT_OF_RANGE, table.GetRetCode());
}

TEST_F(TestTScanTable, cant_find_record) {
  // Arrange
  TScanTable table(N);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], arrVal[i]);

  // Act
  table.FindRecord("a");

  // Assert
  EXPECT_EQ(Data::NO_RECORD, table.GetRetCode());
}

TEST_F(TestTScanTable, cant_delete_record) {
  // Arrange
  TScanTable table(N);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], arrVal[i]);

  // Act
  table.DelRecord("a");

  // Assert
  EXPECT_EQ(Data::NO_RECORD, table.GetRetCode());
}

TEST_F(TestTScanTable, can_get_last_value) {
  // Arrange
  TScanTable table(N);

  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], arrVal[i]);

  // Act
  PTDatValue result = table.GetValuePTR(TDataPos::LAST_POS);

  // Assert
  PTDatValue expected_value = arrVal[N-1];
  EXPECT_EQ(expected_value, result);
}

TEST_F(TestTScanTable, cant_get_error_record) {
  // Arrange
  TScanTable table(N);
  for (int i = 0; i < N; i++)
    table.InsRecord(keys[i], arrVal[i]);

  // Act
  table.FindRecord("1");

  // Assert
  EXPECT_EQ(Data::OK, table.GetRetCode());
}

TEST_F(TestTScanTable, cant_make_navigation) {
  // Arrange
  TScanTable table(1);
  PTDatValue ptrValue = new TString("4");
  table.InsRecord("Red", ptrValue);

  // Act
  for (table.Reset(); !table.IsTabEnded(); table.GoNext())
    ;

  // Assert
  EXPECT_EQ(Data::OUT_OF_RANGE, table.GoNext());
}

TEST_F(TestTScanTable, can_make_navigation) {
  // Arrange
  TScanTable table(N);
  std::set<std::string> expected_set;

  for (int i = 0; i < N; i++) {
    table.InsRecord(keys[i], arrVal[i]);
    expected_set.insert(keys[i]);
  }

  // Act
  std::set<std::string> result_set;
  for (table.Reset(); !table.IsTabEnded(); table.GoNext())
    result_set.insert(table.GetKey());

  // Assert
  EXPECT_EQ(expected_set, result_set);
}