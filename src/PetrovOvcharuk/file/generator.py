
discipl = open('discipline.txt', encoding="utf8")
size = int(discipl.readline())
discipl.close()

print('Введите название файла, в котором сгенерировать случайные оценки')
nameFile = input()

try:
    group = open(nameFile, encoding="utf8")
    out = open('temp.txt','w',encoding="utf8")
    from random import randint

    for line in group:
    	line = line.split()[0] + " "+ " ".join( [str(randint(1,6) ) for i in range(size)])
    	out.write(line+'\n')

    group.close()
    out.close()

    import os
    os.remove(nameFile)
    os.rename('temp.txt',nameFile)
except IOError:
    print ("No file")